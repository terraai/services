package ai.terra.heartbeat;

import io.almostrealism.monitoring.HeartBeatStep;

import java.io.IOException;

public class PythonStep implements HeartBeatStep {
	public void init(String s) { }

	public void run() {
		try {
			Runtime.getRuntime().exec("python heartbeat.py");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
