package ai.terra.heartbeat;

import io.almostrealism.monitoring.HeartBeatStep;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

public class DAGInstaller implements HeartBeatStep {
    private URL dagsUrl;

    @Override
    public void init(String s) {
        try {
            this.dagsUrl = new URL(s);
            this.dagsUrl = new URL(dagsUrl.getProtocol(), dagsUrl.getHost(), dagsUrl.getPort(), "/dags.zip");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            ReadableByteChannel rbc = Channels.newChannel(dagsUrl.openStream());
            FileOutputStream fos = new FileOutputStream("/airflow/dags.zip");

            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            Runtime rt = Runtime.getRuntime();

            Process pr = rt.exec("rm -rf /airflow/dags");
            pr.waitFor();

            pr = rt.exec("unzip /airflow/dags.zip");
            pr.waitFor();
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }
}
