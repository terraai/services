FROM 383046911776.dkr.ecr.us-west-2.amazonaws.com/core:latest
#FROM circleci/openjdk:8-jdk

ENV MAVEN_OPTS -Xmx3200m

EXPOSE 8080:8080
EXPOSE 8793

# Set the working directory to /app
WORKDIR /root

# Copy the current directory contents into root
ADD . /root

RUN wget http://mirror.jax.hugeserver.com/apache/maven/maven-3/3.5.0/binaries/apache-maven-3.5.0-bin.tar.gz
RUN tar -zxvf apache-maven-3.5.0-bin.tar.gz
RUN mkdir ~/.m2
RUN cp settings.xml  ~/.m2/settings.xml
RUN apache-maven-3.5.0/bin/mvn clean install

