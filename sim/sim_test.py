

import sys
from collections import OrderedDict
from random import random
import time
import os

# Custom imports
sys.path.append("\..")



# Config
cfg = OrderedDict(list)


def testing_sim(dir, sim_name="testing.txt", sim_size=1024):

    if not os.path.isdir(dir):
        print 'Making dir'
        os.mkdir(dir)

    wait = random()

    time.sleep(wait)
    print "waited " + str(wait)

    # Write test file
    try:
        with open(dir + sim_name, "wb") as out:
            out.truncate(sim_size)
    except:
        raise TypeError




