# msched - Modeling Scheduler

All provisioning, pipeline, and scheduling

# Stack 
  - Airflow
    - Celery Executor (Mike to prove otherwise) eventually moving to DC/OS
  - Docker
  - Elastic Bean Stalk

    #   AWS Services
        # Security
        - Cognito
        - IAM

  - Cloud Front
  - CloudFormation
  - EFS

# app.py
  - cron / CI
  - boto
    - Check tag
    - EFS
  - ssh
    - Pull keys
  - git
  - Airflow
    - init
    - Make symbolic link for dags
    - webserver & scheduler & worker
  - main

# Docker

To create a new docker image, make sure you run from outside the repo directory (it should be visible with 'ls'):
```sh
docker build -t worker msched
docker run worker
```

Some problems and solutions for docker
```sh
docker-machine env default
eval $("C:\Program Files\Docker Toolbox\docker-machine.exe" env default)
```
Sometimes clean out your docker
```sh
docker system prune
```


# Elastic Bean Stalk (Tomcat)
To deploy the dynamic configuration web app, use maven to package the WAR file for upload to Elastic Bean Stalk:
```sh
cd ModelSchedulerConfig
mvn package
```

This will produce a target directory which contains the WAR file for the application.

# Circle CI

Normally the apps are deployed using Circle CI. As you can see by looking in .circleci/config.yml, the results of maven and webkit packaging the app are synchronized to S3 where they can be used by CodePipeline.

# msched - Modeling Scheduler

