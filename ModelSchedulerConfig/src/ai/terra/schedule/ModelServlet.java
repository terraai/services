package ai.terra.schedule;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/Model")
public class ModelServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String cmd = request.getParameter("cmd");

        if (cmd == null || cmd.length() <= 0) {
            response.setStatus(400);
            response.getWriter().append("No command specified");
        } else if (cmd.equals("new")) {
            // TODO Add from disk option, add from url option
            response.setStatus(200);
            response.getWriter().append("Creating model");
        } else if (cmd.equals("export")) {
            response.setStatus(200);
            response.getWriter().append("Exporting model");
        } else if (cmd.equals("delete")) {
            response.setStatus(200);
            response.getWriter().append("Deleting model");
        } else if (cmd.equals("modify")) {
            response.setStatus(200);
            response.getWriter().append("Modifying model");
        } else if (cmd.equals("export")) {
            response.setStatus(200);
            response.getWriter().append("Exporting model");
        }
    }
}
