package ai.terra.schedule;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/Run")
public class RunServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String cmd = request.getParameter("cmd");
        if (cmd == null || cmd.length() <= 0) {
            response.setStatus(400);
            response.getWriter().append("No command specified");
        } else if (cmd.equals("start")) {
            // TODO Check for "similar"
            response.setStatus(200);
            response.getWriter().append("Starting run");
        } else if (cmd.equals("terminate")) {
            response.setStatus(200);
            response.getWriter().append("Terminiating run");
        } else if (cmd.equals("export")) {
            response.setStatus(200);
            response.getWriter().append("Exporting run");
        }
    }
}
