package ai.terra.schedule;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ai.terra.cellspace.CellSpaceServlet;

@WebServlet("/Project")
public class ProjectServlet extends CellSpaceServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String cmd = request.getParameter("cmd");

        if (cmd == null || cmd.length() <= 0) {
            response.setStatus(400);
            response.getWriter().append("No command specified");
        } else if (cmd.equals("new")) {
            response.setStatus(200);
            response.getWriter().append("Creating project");
            PrintWriter p = new PrintWriter(new OutputStreamWriter(getCellSpace().getOutputStream("/project/description.txt")));
            p.println("This project was created at " + new Date().toString());
        }
    }
}
