package ai.terra.schedule;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/Licenses")
public class LicensesServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String cmd = request.getParameter("cmd");

        if (cmd == null || cmd.length() <= 0) {
            response.setStatus(400);
            response.getWriter().append("No command specified");
        } else if (cmd.equals("add")) {
            response.setStatus(200);
            response.getWriter().append("Adding License");
        } else if (cmd.equals("test")) {
            response.setStatus(200);
            response.getWriter().append("Testing License");
        } else if (cmd.equals("modify")) {
            response.setStatus(200);
            response.getWriter().append("Modifying License");
        }
    }
}
