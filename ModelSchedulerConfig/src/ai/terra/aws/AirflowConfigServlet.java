package ai.terra.aws;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.almostrealism.cloudconfig.AirflowConfig;
import io.almostrealism.cloudconfig.AirflowConfigData;
import io.almostrealism.cloudconfig.AirflowConfigDataFactory;

@WebServlet("/AirflowConfig")
public class AirflowConfigServlet extends AirflowConfig {

	@Override
	protected AirflowConfigData getAirflowConfiguration(HttpServletRequest req) throws IOException {
		String executor = req.getParameter("executor");

		String str = read(req,"/config.json");

		ObjectMapper m = new ObjectMapper();
		JsonNode t = m.readTree(str.toString()).get("airflow");

		AirflowConfigData c = AirflowConfigDataFactory.createConfig(t.toString());

		String profile = req.getParameter("profile");
		if (profile != null && profile.length() > 0) {
			str = read(req, "/airflow_profiles/" + profile + ".json");
			t = m.readTree(str.toString()).get("airflow");
			c.update(t);
		}

		if (executor != null && executor.length() > 0) c.setExecutor(executor);
		return c;
	}

	public String read(HttpServletRequest req, String location) throws IOException {
		StringBuffer str = new StringBuffer();

		URL url = new URL(req.getScheme(), "localhost", req.getServerPort(), location);
		BufferedReader r = new BufferedReader(new InputStreamReader(url.openStream()));

		String line;

		while ((line = r.readLine()) != null) {
			str.append(line);
			str.append("\n");
		}

		return str.toString();
	}
}