package ai.terra.aws;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.almostrealism.html.HTMLPage;

import io.almostrealism.cluster.ClusterManager;

@WebServlet("/ClusterList")
public class ClusterListServlet extends HttpServlet {
	private ClusterManager clusters;
	
	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		clusters = ClusterManager.getDefaultManager();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HTMLPage p = new HTMLPage();
		p.add(clusters.getClusters().toHTML());
		response.getWriter().println(p.toHTML());
	}
}
