package ai.terra.auth;

import java.util.List;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;

import io.almostrealism.auth.AuthenticatableFactory;

public class UserFactory implements AuthenticatableFactory<User> {
	private DynamoDBMapper mapper;

    public void init() {
		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();
		mapper = new DynamoDBMapper(client);
    }

	public User getAuthenticatable(String identifier, String password) {
		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();
		DynamoDBMapper mapper = new DynamoDBMapper(client);
		
		User user = new User();
		user.setUsername(identifier);
		DynamoDBQueryExpression<User> queryExpression = new DynamoDBQueryExpression<User>().withHashKeyValues(user);
		
		List<User> itemList = mapper.query(User.class, queryExpression);
		
		if (itemList.size() == 1) {
			return itemList.get(0);
		} else {
			return null;
		}
	}
}
