package ai.terra.auth;

import java.util.List;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import io.almostrealism.auth.Authenticatable;

@DynamoDBTable(tableName="users")
public class User implements Authenticatable {
	private String id;
	private String username;
	private String firstName, lastName;
	private String password;
	private String email;
	private String phone, fax;
	private String street, city, state;
	private String country, zip;
	private String org;
	private List<String> billingGroups;
	private List<String> allowableServices;
	private List<String> reportingGroups;
	private List<String> accessGroups;
	private List<String> securityGroups;
	private List<String> permissionsProfile;
	private List<String> notes;

	public String getId() { return id; }
	public void setId(String id) { this.id = id; }

	@DynamoDBHashKey(attributeName="username")
	public String getUsername() { return username; }
	public void setUsername(String username) { this.username = username; }
	
	@DynamoDBAttribute(attributeName="first_name")
	public String getFirstName() { return firstName; }
	public void setFirstName(String first) { this.firstName = first; }
	
	@DynamoDBAttribute(attributeName="last_name")
	public String getLastName() { return lastName; }
	public void setLastName(String last) { this.lastName = last; }
	
	@DynamoDBAttribute(attributeName="password")
	public String getPassword() { return password; }
	public void setPassword(String password) { this.password = password; }
	
	@DynamoDBAttribute(attributeName="email")
	public String getEmail() { return email; }
	public void setEmail(String email) { this.email = email; }
	
	@DynamoDBAttribute(attributeName="phone")
	public String getPhone() { return phone; }
	public void setPhone(String phone) { this.phone = phone; }
	
	@DynamoDBAttribute(attributeName="fax")
	public String getFax() { return fax; }
	public void setFax(String fax) { this.fax = fax; }

	@DynamoDBAttribute(attributeName="street")
	public String getStreet() { return street; }
	public void setStreet(String street) { this.street = street; }

	@DynamoDBAttribute(attributeName="city")
	public String getCity() { return city; }
	public void setCity(String city) { this.city = city; }

	@DynamoDBAttribute(attributeName="state")
	public String getState() { return state; }
	public void setState(String state) { this.state = state; }

	@DynamoDBAttribute(attributeName="country")
	public String getCountry() { return country; }
	public void setCountry(String country) { this.country = country; }
	
	@DynamoDBAttribute(attributeName="zip")
	public String getZip() { return zip; }
	public void setZip(String zip) { this.zip = zip; }

	@DynamoDBAttribute(attributeName="org")
	public String getOrg() { return org; }
	public void setOrg(String org) { this.org = org; }
	
	@DynamoDBAttribute(attributeName="billing_groups")
	public List<String> getBillingGroups() { return billingGroups; }
	public void setBillingGroups(List<String> billingGroups) { this.billingGroups = billingGroups; }
	
	@DynamoDBAttribute(attributeName="allowable_services")
	public List<String> getAllowableServices() { return allowableServices; }
	public void setAllowableServices(List<String> allowableServices) { this.allowableServices = allowableServices; }

	@DynamoDBAttribute(attributeName="reporting_groups")
	public List<String> getReportingGroups() { return reportingGroups; }
	public void setReportingGroups(List<String> reportingGroups) { this.reportingGroups = reportingGroups; }

	@DynamoDBAttribute(attributeName="access_groups")
	public List<String> getAccessGroups() { return accessGroups; }
	public void setAccessGroups(List<String> accessGroups) { this.accessGroups = accessGroups; }

	@DynamoDBAttribute(attributeName="security_groups")
	public List<String> getSecurityGroups() { return securityGroups; }
	public void setSecurityGroups(List<String> securityGroups) { this.securityGroups = securityGroups; }

	@DynamoDBAttribute(attributeName="permissions_profile")
	public List<String> getPermissionsProfile() { return permissionsProfile; }
	public void setPermissionsProfile(List<String> permissionsProfile) { this.permissionsProfile = permissionsProfile; }

	@DynamoDBAttribute(attributeName="notes")
	public List<String> getNotes() { return notes; }
	public void setNotes(List<String> notes) { this.notes = notes; }
}
