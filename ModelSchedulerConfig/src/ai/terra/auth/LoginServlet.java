package ai.terra.auth;

import io.almostrealism.auth.AuthenticationServlet;

import javax.servlet.annotation.WebServlet;

/**
 * Concrete implementation of {@link AuthenticationServlet} which uses DynamoDB
 * (via {@link UserFactory}) to check user credentials.
 * public LoginServlet() { super(USE_COGNITO ? new CognitoUserFactory() : new UserFactory(); }
 */
@WebServlet("/Login")
public class LoginServlet extends AuthenticationServlet<User> {

	private static final long serialVersionUID = 1L;

    public LoginServlet() { super(USE_COGNITO ? new CognitoUserFactory() : new UserFactory()); }

	public static final boolean USE_COGNITO = true;
}
