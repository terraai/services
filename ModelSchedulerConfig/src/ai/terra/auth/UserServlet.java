package ai.terra.auth;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/User")
public class UserServlet  extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String cmd = request.getParameter("cmd");

        if (cmd == null || cmd.length() <= 0) {
            response.setStatus(400);
            response.getWriter().append("No command specified");
        } else if (cmd.equals("update")) {
            response.setStatus(200);
            response.getWriter().append("Updated user");
        }
    }
}
