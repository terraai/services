package ai.terra.auth;

import ai.terra.crypto.EncryptionService;
import io.almostrealism.auth.AuthenticatableFactory;

import java.security.NoSuchAlgorithmException;

public class CognitoUserFactory implements AuthenticatableFactory<User> {
	private EncryptionService e;

	public void init() {
		try {
			e = new EncryptionService("AKIAII4PQOXCUBEPF5TA", "I/+Sygl8d26zjTOBKDTJt7O8QErihv1TmRYMVQVn",
//					"4t5mj6f1go0rff8hbo65dn6vi3", "bfr12irv0hg3bm56mp241h2erhdmahqbmskbnhbojq0f29i4eem",
					"AKIAII4PQOXCUBEPF5TA", "I/+Sygl8d26zjTOBKDTJt7O8QErihv1TmRYMVQVn",
					true);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

	}

	public User getAuthenticatable(String identifier, String password) {
		return e.getId().checkPassword(identifier, password) ? new User() : null;
	}
}
