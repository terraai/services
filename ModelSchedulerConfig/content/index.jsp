<!DOCTYPE html>
<%@page import="io.almostrealism.cluster.ClusterManager"%>
<%@page import="io.almostrealism.cluster.ARCluster"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    <div id="app"></div>

    <script src="bundle.js"></script>

    <script src="https://unpkg.com/react@15/dist/react.js"></script>
    <script src="https://unpkg.com/react-dom@15/dist/react-dom.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.8.34/browser.js"></script>

    <script type="text/babel">

        // React is a global from the React library
        var div = React.DOM.div; // let's create a div!
        var h1 = React.DOM.h1; // let's create an h1!

        class MyComponent extends React.Component {
            render() {
                return div(
                    null,
                    h1(null, "My very first component!")
                );
            }
        }

        var App = (
            div(null,
                h1({id: "page-title"}, 'Hello World!'),
                // now lets add our component!
                React.createElement(MyComponent, null)
            )
        );

        // lets put this on the DOM
        ReactDOM.render(App, document.getElementById("app"));
    </script>


    <script type="text/babel">
        function App() {
        		return (
        			<div>
        				<% for (ARCluster c : ClusterManager.getDefaultManager().getClusters()) { %>
        					<Cluster name="<%= c.getName() %>" />
        				<% } %>
        			</div>
        		);
        }

        // lets put this on the DOM
        ReactDOM.render(<App/>, document.getElementById("app"));
    </script>
</body>
</html>
